# MACD策略
import talib
import module_backtest
from vnctptdType661 import *
from PyQt5 import QtCore
# CTP行情库
from vnctpmd import *
import numpy as np
import globalType
import globalvar

parlist = [['fastperiod',3,5,1],['slowperiod',15,30,1],['signalperiod',15,30,1]]

class MyStrategy(module_backtest.VirtualAccount, QtCore.QThread):
    def __init__(self, period,slippoint):
        super(MyStrategy, self).__init__(period,slippoint)
        self.close = []
        self.type = TradeType_VIRTUALACCOUNT

    def OnKline(self, mddata, arg, strategyname):
        if arg[0] <= 0 or arg[1] <= 0:
            return
        # TradingDay = klinedata.TradingDay.decode()
        # klinetime = klinedata.klinetime.decode()
        self.InstrumentID = mddata.InstrumentID.decode()
        # self.exchange=mddata.exchange.decode()
        self.close.append(float(mddata.close))
        try:
            float_close = [float(x) for x in self.close]
        except Exception as e:
            pass
        # 默认参数 12,26,9
        #DIF是12日指数移动平均线(EMA12，又称快线)与26日指数移动平均线（EMA26，又称慢线）的背离程度：
        #DEA是DIF的9日指数移动平均线：
        #HIST是DIF与DEA的差值乘以2：
        #signal是MACD线周期为9的EMA：
        macd, signal, hist = talib.MACD(np.array(float_close), fastperiod=arg[0], slowperiod=arg[1], signalperiod=arg[2])
        #当天macd
        macd0 = macd[len(macd) - 1]
        #当天single
        signal0 = signal[len(signal) - 1]
        # 前一个交易日macd
        macd1 = macd[len(macd) - 1]
        # 前一个交易日single
        signal1 = signal[len(signal) - 1]

        if macd1 < signal1 and  macd0 > signal0 :
            # 金叉
            if self.sellvol + self.sellvol_history > 0:
                self.InsertOrder(self.InstrumentID, '', THOST_FTDC_D_Buy, THOST_FTDC_OF_Close, VN_OPT_LimitPrice,
                                 mddata.close + 1, 1)
            if self.buyvol + self.buyvol_history < 10:
                self.InsertOrder(self.InstrumentID, '', THOST_FTDC_D_Buy, THOST_FTDC_OF_Open, VN_OPT_LimitPrice,
                                 mddata.close + 1, 1)
        elif macd1 > signal1 and  macd0 < signal0 :
            # 死叉
            if self.buyvol + self.buyvol_history > 0:
                self.InsertOrder(self.InstrumentID, '', THOST_FTDC_D_Sell, THOST_FTDC_OF_Close, VN_OPT_LimitPrice,
                                 mddata.close - 1, 1)
            if self.sellvol + self.sellvol_history < 10:
                self.InsertOrder(self.InstrumentID, '', THOST_FTDC_D_Sell, THOST_FTDC_OF_Open, VN_OPT_LimitPrice,
                                 mddata.close - 1, 1)






import copy

from PyQt5.QtWidgets import *
from pyqtgraph import QtCore, QtGui
from PyQt5.QtWidgets import QPushButton
import os
import configparser
import globalvar
from datetime import timedelta
from datetime import datetime
import multiprocessing
import time
import importlib
from vnctpmdType661 import *
from PyQt5.QtCore import Qt, pyqtSignal
from pathlib import Path
from multiprocessing import Process, Queue

global dict_instrumentgroup, dict_instrumentgrouprun
dict_instrumentgroup = {}
dict_instrumentgrouprun = {}
import globalType
import threading
import random


# 用于回测的虚拟账号父类
class VirtualAccount(object):
    def __init__(self, period, slipoint):
        self.period = period
        self.dict_strategyinstrument = {}
        self.dick_tick = {}
        self.list_tick = []
        self.fee = 0
        self.buyposition = 0
        self.sellposition = 0
        self.slippoint = slipoint
        self.jump = 1
        self.MA_A = 0
        self.MA_B = 0
        self.exchange = 'SHFE'
        # self.InstrumentID
        self.feetype = VN_FEE_RATE
        self.buyvol = 0
        self.sellvol = 0
        self.buyvol_history = 0
        self.sellvol_history = 0
        self.buyprice = 0
        self.sellprice = 0
        self.LastPrice = 0
        self.initamount = 500000
        self.amount = self.initamount
        self.useramount = self.initamount
        self.BidPrice = 0
        self.AskPrice = 0
        self.todayCloseProfit = 0
        self.todayPositionProfit = 0
        self.todayPreBalance = 0
        self.totalfee = 0
        self.buyopennum = 0
        self.sellopennum = 0
        self.buyclosenum = 0
        self.sellclosenum = 0
        self.feetype = VN_FEE_RATE
        self.OpenFee = 0
        self.CloseFee = 0

    def evaluation(self):
        return [1, 1, 1, 1, 1]

    dict_cx = {'rb': [10 * 0.1, 10],
               'RM': [10 * 0.1, 10],
               'ru': [10 * 0.14, 10],
               'SM': [5 * 0.10, 5],
               'CF': [5 * 0.10, 5],
               'zn': [5 * 0.11, 5],
               'al': [5 * 0.09, 5],
               'cu': [5 * 0.14, 5],
               'au': [1000 * 0.12, 1000],
               'ag': [15 * 0.14, 15],
               'hc': [10 * 0.1, 10],
               'bu': [10 * 0.13, 10],
               'ni': [1 * 0.14, 1],
               'i': [100 * 0.11, 100],
               'TA': [5 * 0.12, 5],
               'a': [10 * 0.15, 10],
               'm': [10 * 0.1, 10],
               'y': [10 * 0.1, 10],
               'p': [5 * 0.12, 5],
               'MA': [10 * 0.10, 10],
               'pp': [5 * 0.11, 5],
               'cs': [10 * 0.08, 10],
               'fg': [20 * 0.1, 20],
               'l': [5 * 0.12, 5],
               'v': [5 * 0.12, 5],
               'SR': [10 * 0.10, 10],
               'c': [10 * 0.10, 10],
               'SF': [5 * 0.10, 5],
               'IC': [200 * 0.12, 200],
               'IF': [300 * 0.12, 300],
               'IH': [300 * 0.12, 300],
               'T': [10000 * 0.03, 10000],
               'TF': [10000 * 0.03, 10000],
               'TS': [10000 * 0.03, 10000],
               'j': [100 * 0.15, 100],
               'CJ': [5 * 0.10, 5],
               'jd': [5 * 0.12, 5],
               'AP': [10 * 0.12, 10],
               'sp': [10 * 0.12, 10],
               'CY': [5 * 0.12, 5],
               'eg': [10 * 0.12, 10]}

    # rm,wh

    def GetCX(self, InstrumentID):
        return self.dict_cx[InstrumentID.strip('0123456789')]

    def InsertOrder(self, InstrumentID, exchangeid, direction, offside, VN_OPT_LimitPrice, price, vol):
        if self.type == TradeType_ACCOUNT:
            # 实盘
            globalvar.td.InsertOrder(InstrumentID, exchangeid, direction, offside, VN_OPT_LimitPrice, price, vol)
        elif self.type == TradeType_VIRTUALACCOUNT:
            # Python回测
            self.InsertOrder_backtest(InstrumentID, exchangeid, direction, offside, VN_OPT_LimitPrice, price, vol)

    def InsertOrder_backtest(self, InstrumentID, ExchangeID, direction, OffsetFlag, OrderPriceType, price, thisvol):
        # 获得该合约参数
        self.askprice = price
        self.bidprice = price
        self.buyprice = price
        self.sellprice = price
        try:
            c = self.GetCX(self.InstrumentID)
        except Exception as e:
            print('GetCX error:', e)
            return
        cx = c[0]
        cy = c[1]
        self.fee = 0;
        if self.feetype == VN_FEE_RATE:
            # 按比例
            if direction == THOST_FTDC_D_Buy:
                if OffsetFlag == THOST_FTDC_OF_Open:
                    self.fee = self.OpenFee * self.askprice * cy
                else:
                    self.fee = self.CloseFee * self.askprice * cy
            else:
                if OffsetFlag == THOST_FTDC_OF_Open:
                    self.fee = self.OpenFee * self.bidprice * cy
                else:
                    self.fee = self.CloseFee * self.bidprice * cy
        elif self.feetype == VN_FEE_CONSTANT:
            # 固定金额
            if OffsetFlag == THOST_FTDC_OF_Open:
                self.fee = self.OpenFee
            else:
                self.fee = self.CloseFee
        # printf("fee:%f\n", fee);
        # self.fee = fee;
        if OffsetFlag == THOST_FTDC_OF_Open:
            # print("z_open\n");
            allownum = float(thisvol)
            # zmin(thisvol, CalUpdateBigAmount(0, addslip_bidprice, self.useramount, & cx, & cy));
            allownumInt = thisvol
            if direction == THOST_FTDC_D_Buy:
                self.buyopennum += 1
                # printf("买开%d\n", self.buyvol);
                self.addslip_askprice = self.askprice + self.slippoint * self.jump
                cz = float(self.buyvol + allownumInt);
                # 保证有足够的资金开新仓
                if self.useramount - allownum * (self.addslip_askprice * cx + self.fee) > 1e-7:
                    if cz > 1e-7:
                        self.buyprice = (self.buyprice * float(
                            self.buyvol + self.buyvol_history) + self.addslip_askprice * allownum) / cz
                        self.buyvol = self.buyvol + allownumInt;
                        # amount = amount - allownum * fee;
                        self.useramount = self.useramount - allownum * (self.addslip_askprice * cx + self.fee)
                        self.totalfee = self.totalfee + allownum * self.fee
            else:
                self.sellopennum += 1
                # printf("卖开%d\n", self.sellvol);
                self.addslip_bidprice = self.bidprice - self.slippoint * self.jump
                cz = float(self.sellvol + allownumInt)
                # 保证有足够的资金开新仓
                if self.useramount - allownum * (self.addslip_bidprice * cx + self.fee) > 1e-7:
                    if cz > 1e-7:
                        self.sellprice = (self.sellprice * float(
                            self.sellvol + self.sellvol_history) + self.addslip_bidprice * allownum) / cz
                        self.sellvol = self.sellvol + allownumInt
                        # amount = amount - allownum * fee
                        self.useramount = self.useramount - allownum * (self.addslip_bidprice * cx + self.fee)
                        self.totalfee = self.totalfee + allownum * self.fee
        elif OffsetFlag == THOST_FTDC_OF_Close or OffsetFlag == THOST_FTDC_OF_CloseToday:
            # print("z_close\n");
            if direction == THOST_FTDC_D_Buy:
                # print("z_多\n")
                self.buyclosenum += 1
                # 买平，平今
                if OffsetFlag == THOST_FTDC_OF_CloseToday:
                    # print("z_平今[%d>%d]\n", self.sellvol, thisvol);
                    if self.sellvol >= thisvol:
                        # print("z_条件\n");
                        allownum = float(thisvol)
                        allownumInt = thisvol;
                        # 资金曲线，持仓并不是动态的，平仓才earn
                        addslip_askprice = self.askprice + float(self.slippoint) * float(self.jump)
                        thisCloseProfit = allownum * (cy * (self.sellprice - addslip_askprice) - self.fee)
                        self.sellvol = self.sellvol - thisvol;
                        self.useramount = self.useramount + allownum * (self.sellprice * cx) + thisCloseProfit
                        self.totalfee = self.totalfee + allownum * self.fee;
                        # 新增平仓权益计算
                        self.todayCloseProfit = self.todayCloseProfit + thisCloseProfit
                        # 新增持仓权益计算F
                        self.todayPositionProfit = (self.sellprice - price) * float(
                            self.sellvol + self.sellvol_history) + \
                                                   (price - self.buyprice) * float(self.buyvol + self.buyvol_history)
                        if self.sellvol == 0 and self.sellvol_history == 0:
                            self.sellprice = 0;
                    else:
                        pass
                        # 下单失败回调
                else:
                    # print("z_平昨[%d>%d]\n", self.sellvol_history, thisvol)
                    if ExchangeID == "SHFE":
                        # 买平，平仓
                        if self.sellvol_history >= thisvol:
                            allownum = float(thisvol)
                            allownumInt = thisvol
                            # 资金曲线，持仓并不是动态的，平仓才earn
                            self.addslip_askprice = self.askprice + float(self.slippoint) * self.jump
                            # 本次盈利
                            self.thisCloseProfit = allownum * (cy * (self.sellprice - self.addslip_askprice) - self.fee)
                            self.sellvol_history = self.sellvol_history - thisvol
                            self.useramount = self.useramount + allownum * (self.sellprice * cx) + self.thisCloseProfit
                            self.totalfee = self.totalfee + allownum * self.fee
                            # 新增平仓权益计算
                            self.todayCloseProfit = self.todayCloseProfit + self.thisCloseProfit
                            # 新增持仓权益计算F
                            self.todayPositionProfit = (self.sellprice - price) * float(
                                self.sellvol + self.sellvol_history) + \
                                                       (price - self.buyprice) * float(
                                self.buyvol + self.buyvol_history)
                            if self.sellvol == 0 and self.sellvol_history == 0:
                                self.sellprice = 0;
                        else:
                            pass
                            # 下单失败回调
                    else:
                        # 买平，平仓
                        if self.sellvol_history >= thisvol:
                            # print("z_条件\n");
                            allownum = float(thisvol)
                            allownumInt = thisvol
                            # 资金曲线，持仓并不是动态的，平仓才earn
                            self.addslip_askprice = self.askprice + float(self.slippoint) * self.jump
                            self.thisCloseProfit = allownum * (
                                    cy * (self.sellprice - self.addslip_askprice) - self.fee)
                            self.sellvol_history = self.sellvol_history - thisvol
                            self.useramount = self.useramount + allownum * (
                                    self.sellprice * cx) + self.thisCloseProfit
                            self.totalfee = self.totalfee + allownum * self.fee
                            # 新增平仓权益计算
                            self.todayCloseProfit = self.todayCloseProfit + self.thisCloseProfit
                            # 新增持仓权益计算F
                            self.todayPositionProfit = (self.sellprice - price) * float(
                                self.sellvol + self.sellvol_history) + (price - self.buyprice) * float(
                                self.buyvol + self.buyvol_history)
                            if self.sellvol == 0 and self.sellvol_history == 0: self.sellprice = 0;
                        elif self.sellvol >= thisvol:
                            # print("z_条件\n");
                            allownum = float(thisvol);
                            allownumInt = thisvol;
                            # 资金曲线，持仓并不是动态的，平仓才earn
                            self.addslip_askprice = self.askprice + float(self.slippoint) * self.jump
                            self.thisCloseProfit = allownum * (
                                    cy * (self.sellprice - self.addslip_askprice) - self.fee)
                            self.sellvol = self.sellvol - thisvol
                            self.useramount = self.useramount + allownum * (
                                    self.sellprice * cx) + self.thisCloseProfit
                            self.totalfee = self.totalfee + allownum * self.fee
                            # 新增平仓权益计算
                            self.todayCloseProfit = self.todayCloseProfit + self.thisCloseProfit
                            # 新增持仓权益计算F
                            self.todayPositionProfit = (self.sellprice - price) * float(
                                self.sellvol + self.sellvol_history) + \
                                                       (price - self.buyprice) * float(
                                self.buyvol + self.buyvol_history);

                            if self.sellvol == 0 and self.sellvol_history == 0:
                                self.sellprice = 0
                        else:
                            pass
                            # 下单失败回调
            else:
                self.sellclosenum += 1
                if OffsetFlag == THOST_FTDC_OF_CloseToday:
                    # print("z_平今[%d>%d]\n", self.buyvol, thisvol)
                    # 卖平，平今
                    if self.buyvol >= thisvol:
                        # print("z_条件")
                        allownum = float(thisvol)
                        allownumInt = thisvol
                        self.addslip_bidprice = self.bidprice - float(self.slippoint) * self.jump
                        self.thisCloseProfit = allownum * (cy * (self.addslip_bidprice - self.buyprice) - self.fee)
                        self.buyvol = self.buyvol - thisvol
                        self.useramount = self.useramount + allownum * (self.buyprice * cx) + self.thisCloseProfit
                        self.totalfee = self.totalfee + allownum * self.fee
                        # 新增收盘权益计算
                        self.todayCloseProfit = self.todayCloseProfit + self.thisCloseProfit
                        # 新增持仓权益计算F
                        self.todayPositionProfit = (self.sellprice - price) * float(
                            self.sellvol + self.sellvol_history) + (price - self.buyprice) * float(
                            self.buyvol + self.buyvol_history)
                        if self.buyvol == 0 and self.buyvol_history == 0:
                            self.buyprice = 0
                    else:
                        # 下单失败回调
                        pass
                else:
                    # print("z_平昨[%d>%d]\n", self.buyvol_history, thisvol)
                    if ExchangeID == "SHFE":
                        # 卖平，平仓
                        if self.buyvol_history >= thisvol:
                            print("z_条件\n");
                            allownum = float(thisvol);
                            allownumInt = thisvol;
                            addslip_bidprice = self.bidprice - float(self.slippoint) * self.jump;
                            thisCloseProfit = allownum * (cy * (addslip_bidprice - self.buyprice) - self.fee)
                            self.buyvol_history = self.buyvol_history - thisvol
                            self.useramount = self.useramount + allownum * (self.buyprice * cx) + thisCloseProfit
                            self.totalfee = self.totalfee + thisvol * self.fee
                            # 新增收盘权益计算
                            self.todayCloseProfit = self.todayCloseProfit + thisCloseProfit
                            # 新增持仓权益计算F
                            self.todayPositionProfit = (self.sellprice - price) * float(
                                self.sellvol + self.sellvol_history) + (price - self.buyprice) * float(
                                self.buyvol + self.buyvol_history)
                            if self.buyvol == 0 and self.buyvol_history == 0:
                                self.buyprice = 0
                        else:
                            # 下单失败回调
                            pass
                    else:
                        # 卖平，平仓
                        if self.buyvol_history >= thisvol:
                            # print("z_条件\n")
                            allownum = float(thisvol)
                            allownumInt = thisvol;
                            self.addslip_bidprice = self.bidprice - float(self.slippoint) * self.jump
                            self.thisCloseProfit = allownum * (cy * (self.addslip_bidprice - self.buyprice) - self.fee)
                            self.buyvol_history = self.buyvol_history - thisvol;
                            self.useramount = self.useramount + allownum * (self.buyprice * cx) + self.thisCloseProfit
                            self.totalfee = self.totalfee + thisvol * self.fee
                            # 新增收盘权益计算
                            self.todayCloseProfit = self.todayCloseProfit + self.thisCloseProfit
                            # 新增持仓权益计算F
                            self.todayPositionProfit = (self.sellprice - price) * float(
                                self.sellvol + self.sellvol_history) + (price - self.buyprice) * float(
                                self.buyvol + self.buyvol_history)
                            if self.buyvol == 0 and self.buyvol_history == 0:
                                self.buyprice = 0
                        elif self.buyvol >= thisvol:
                            # print("z_条件\n")
                            allownum = float(thisvol)
                            allownumInt = thisvol
                            self.addslip_bidprice = self.bidprice - float(self.slippoint * self.jump)
                            self.thisCloseProfit = allownum * (cy * (self.addslip_bidprice - self.buyprice) - self.fee)
                            self.buyvol = self.buyvol - thisvol
                            self.useramount = self.useramount + allownum * (self.buyprice * cx) + self.thisCloseProfit
                            self.totalfee = self.totalfee + thisvol * self.fee
                            # 新增收盘权益计算
                            self.todayCloseProfit = self.todayCloseProfit + self.thisCloseProfit
                            # 新增持仓权益计算F
                            self.todayPositionProfit = (self.sellprice - price) * float(
                                self.sellvol + self.sellvol_history) + (price - self.buyprice) * float(
                                self.buyvol + self.buyvol_history)
                            if self.buyvol == 0 and self.buyvol_history == 0:
                                self.buyprice = 0
                        else:
                            pass
                            # 下单失败回调

    def checkinstrumentID(self, marketdata, strategyname):
        if strategyname in globalvar.dict_strategyinstrument:
            if str(marketdata.InstrumentID, encoding="utf-8") in globalvar.dict_strategyinstrument[strategyname]:
                return 0
            else:
                return 1
        else:
            return 1


# 从文件读取数据文件信息
def Function_ReadDataList(tablename, showcheck):
    datafilenamelist = []
    with open('backtestdata//datalist.csv', 'r') as f:
        for line in f:
            # c_path = os.path.join(path, i)
            dataarr = line.strip('\n').split(',')
            datafilenamelist.append(list(line.strip('\n').split(',')))
            # self.UpdateMainType(dataarr[1], dataarr[2], dataarr[3], dataarr[4])
            row_cnt = tablename.rowCount()  # 返回当前行数（尾部）
            # print("列数：",row_cnt)
            tablename.insertRow(row_cnt)  # 尾部插入一行新行表格
            column_cnt = tablename.columnCount()  # 返回当前列数
            # for column in range(column_cnt):
            item = QTableWidgetItem(str(row_cnt + 1))
            tablename.setItem(row_cnt, 0, item)
            item = QTableWidgetItem(dataarr[0])
            if showcheck:
                item.setCheckState(QtCore.Qt.Checked)
            tablename.setItem(row_cnt, 1, item)
            item = QTableWidgetItem(dataarr[1])
            tablename.setItem(row_cnt, 2, item)
            item = QTableWidgetItem(dataarr[2])
            tablename.setItem(row_cnt, 3, item)
            item = QTableWidgetItem(dataarr[3])
            tablename.setItem(row_cnt, 4, item)
            item = QTableWidgetItem(dataarr[4])
            tablename.setItem(row_cnt, 5, item)
            Button1 = QPushButton("下载数据")
            # self.deleteButtonlist.append(self.deleteButton)
            # 暂时屏蔽 deleteButton.clicked.connect(self.Function_Clicked_EditInstrumentID)
            # buttonid = buttonid + 1
            tablename.setCellWidget(row_cnt, 7, Button1)


'''
# 更新回测资金曲线
class UIUpdatebacktestThread(threading.Thread):
    def __init__(self, newdata):
        super(UIUpdatebacktestThread, self).__init__()
        self.newdata = newdata

    def run(self):
        globalvar.ui.updatebacktestUi(self.newdata)
'''


# 量化回测窗口
class BackTest(object):
    def __init__(self, signal_backtest_processbar, signal_backtest_result, signal_backtest_loaddata):
        self.signal_backtest_processbar = signal_backtest_processbar
        self.signal_backtest_result = signal_backtest_result
        self.signal_backtest_loaddata = signal_backtest_loaddata


class BackTestThread(QtCore.QThread):
    signal_backtest_processbar = pyqtSignal(list)
    signal_backtest_result = pyqtSignal(list)
    signal_backtest_loaddata = pyqtSignal(list)

    def __del__(self):
        self.wait()

    def __init__(self, tname):
        super(BackTestThread, self).__init__()
        globalvar.BackTestThreadPoint = self
        self.tname = tname
        # 存储所有参数组合
        self.parlist = []
        globalvar.DialogBackTestPoint.closestate = False
        self.starttime = 0

    def run(self):
        pass

    def BackTestUpdateResultThread2(self, q):
        while 1:
            print('BackTestUpdateResultThread')
            time.sleep(1)

        while True:
            # while globalvar.backtestdealprocessstate:
            print("S1")
            QApplication.processEvents()
            # self.lock.acquire()
            # qisempty = self.q.empty()
            # self.lock.release()
            print("S2")
            if qisempty:
                # globalvar.vnfa.AsynSleep(1000)
                if globalvar.totaltasknum > 0:
                    if globalvar.totaltasknum == globalvar.finishtasknum:
                        if globalvar.BackTestThreadPoint.closestate == False:
                            globalvar.BackTestThreadPoint.closestate = True
                            globalvar.DialogBackTestPoint.backteststate = True
                            globalvar.DialogBackTestPoint.table_lefttop.setEditTriggers(QTableView.CurrentChanged)
                            print(str(globalvar.totaltasknum))
                            globalvar.BackTestThreadPoint.OnStop('完成回测')
                            break

                globalvar.vnfa.AsynSleep(200)
                print("S3")
            else:
                print("S4")
                QApplication.processEvents()
                self.lock.acquire()
                result = self.q.get(True)
                self.lock.release()
                print("BackTestUpdateResultThread: " + str(result))
                print("S5")
                globalvar.DialogBackTestPoint.bt.signal_backtest_result.emit(result)
                print("S6")
                '''
                if 1:
                    # try:
                    if globalvar.BackTestThreadPoint.closestate == True:
                        globalvar.finishtasknum = globalvar.finishtasknum + 1
                        return
                    globalvar.finishtasknum = globalvar.finishtasknum + 1
                    if globalvar.totaltasknum == 0:
                        return

                    if globalvar.totaltasknum > 0:
                        rate = float(globalvar.finishtasknum) / float(globalvar.totaltasknum)
                        diffsecond = (datetime.now() - self.starttime).seconds
                        h = int(diffsecond / 3600)
                        m = int((diffsecond - h * 3600) / 60)
                        s = diffsecond - h * 3600 - m * 60
                        if rate > 0:
                            leftsecond = int(float(diffsecond) * ((1.0 - rate) / rate))
                        else:
                            leftsecond = 0
                        h2 = int(leftsecond / 3600)
                        m2 = int((leftsecond - h2 * 3600) / 60)
                        s2 = leftsecond - h2 * 3600 - m2 * 60

                        usertime = "耗时%d:%02d:%02d 完成倒计时%d:%02d:%02d" % (h, m, s, h2, m2, s2)
                        thisvalue = float(100 * rate)
                        formattext = "回测总任务(参数组)进度：%p% (" + str(globalvar.finishtasknum) + "\\" + str(
                            globalvar.totaltasknum) + ")" + usertime
                        globalvar.DialogBackTestPoint.bt.signal_backtest_processbar.emit([thisvalue, formattext])

                '''

                # except Exception as e:
                #    print('OnFinish error:', e)
                globalvar.vnfa.AsynSleep(0)

    class BackTestUpdateResultThread(threading.Thread):
        def __del__(self):
            self.wait()

        def __init__(self):
            super(self).__init__()

        def run(self):
            time.sleep(1)
            globalvar.md.ui = globalvar.ui

    class myThread(threading.Thread):
        def __init__(self, threadID, name, counter, threadLock):
            threading.Thread.__init__(self)
            self.threadID = threadID
            self.name = name
            self.counter = counter
            self.threadLock = threadLock

        def run(self):
            print("开启线程： " + self.name)
            # 获取锁，用于线程同步
            self.threadLock.acquire()
            self.print_time(self.name, self.counter, 3)
            # 释放锁，开启下一个线程
            self.threadLock.release()

        def print_time(self, threadName, delay, counter):
            while counter:
                # time.sleep(delay)
                if threadName == 'Thread-1':
                    globalvar.vnfa.AsynSleep(random.randint(0, 9000))
                else:
                    globalvar.vnfa.AsynSleep(random.randint(0, 1000))
                QApplication.processEvents()
                print("%s: %s" % (threadName, time.ctime(time.time())))
                counter -= 1

    def Initprocess(self, processnum, slippoint, period):
        bt = globalvar.BackTestThreadPoint.BackTestThreadMangement(processnum)
        bt.RunBackTest(slippoint, period)

    def OnStart(self, parnum, conditions0, conditions1, conditions2):
        globalvar.DialogBackTestPoint.backteststate = True
        globalvar.DialogBackTestPoint.table_lefttop.setEditTriggers(QTableView.NoEditTriggers)
        globalvar.DialogBackTestPoint.btn_ok.setStyleSheet("QPushButton{border-image: url(onstopbacktest1.png)}")
        globalvar.DialogBackTestPoint.list_backtestlog.clear()
        globalvar.DialogBackTestPoint.taskprogressBar.setProperty("value", 0)
        globalvar.DialogBackTestPoint.taskprogressBar.setFormat("回测总任务(参数组)进度：0%")
        globalvar.DialogBackTestPoint.Log("开始测试【" + globalvar.DialogBackTestPoint.strategyname + '】')
        for i in range(globalvar.DialogBackTestPoint.table_leftbottom.rowCount()):
            if globalvar.DialogBackTestPoint.table_leftbottom.item(i, 1).checkState():
                globalvar.DialogBackTestPoint.Log(
                    '将回测：' + globalvar.DialogBackTestPoint.table_leftbottom.item(i, 1).text() + ' ,时间段：' +
                    globalvar.DialogBackTestPoint.table_leftbottom.item(i,
                                                                        2).text() + '~' + globalvar.DialogBackTestPoint.table_leftbottom.item(
                        i,
                        3).text())
        parnumlist = [0, 0, 0, 0, 0, 0]
        for rowid in range(parnum):
            begin = int(globalvar.DialogBackTestPoint.table_lefttop.item(rowid, 1).text())
            end = int(globalvar.DialogBackTestPoint.table_lefttop.item(rowid, 2).text())
            step = int(globalvar.DialogBackTestPoint.table_lefttop.item(rowid, 3).text())
            parnumlist[rowid] = (end - begin + 1) / max(1, step)

        globalvar.totaltasknum = int(
            max(1, parnumlist[0]) * max(1, parnumlist[1]) * max(1, parnumlist[2]) * max(1, parnumlist[3]) * max(1,
                                                                                                                parnumlist[
                                                                                                                    4]) * max(
                1, parnumlist[5]))

        globalvar.finishtasknum = 0
        self.parlist = []
        globalvar.DialogBackTestPoint.Log('回测选项：' + globalvar.DialogBackTestPoint.comBox_process.currentText())
        globalvar.DialogBackTestPoint.Log('待回测参数组：【' + str(globalvar.totaltasknum) + '】个')
        processnum = min(globalvar.DialogBackTestPoint.comBox_process.currentIndex() + 1, globalvar.totaltasknum)
        slippoint = globalvar.DialogBackTestPoint.comBox_slippoint.currentIndex() + 1
        period = globalvar.DialogBackTestPoint.comBox_period.currentText()

        print('processnum: ' + str(processnum))
        if globalvar.DialogBackTestPoint.table_lefttop.item(0, 1):
            for parnumlist[0] in range(int(globalvar.DialogBackTestPoint.table_lefttop.item(0, 1).text()),
                                       int(globalvar.DialogBackTestPoint.table_lefttop.item(0, 2).text()) + 1,
                                       int(globalvar.DialogBackTestPoint.table_lefttop.item(0, 3).text())):
                if globalvar.DialogBackTestPoint.table_lefttop.item(1, 1):
                    for parnumlist[1] in range(int(globalvar.DialogBackTestPoint.table_lefttop.item(1, 1).text()),
                                               int(globalvar.DialogBackTestPoint.table_lefttop.item(1, 2).text()) + 1,
                                               int(globalvar.DialogBackTestPoint.table_lefttop.item(1, 3).text())):
                        if globalvar.DialogBackTestPoint.table_lefttop.item(2, 1):
                            for parnumlist[2] in range(
                                    int(globalvar.DialogBackTestPoint.table_lefttop.item(2, 1).text()),
                                    int(globalvar.DialogBackTestPoint.table_lefttop.item(2, 2).text()) + 1,
                                    int(globalvar.DialogBackTestPoint.table_lefttop.item(2, 3).text())):
                                if globalvar.DialogBackTestPoint.table_lefttop.item(3, 1):
                                    for parnumlist[3] in range(
                                            int(globalvar.DialogBackTestPoint.table_lefttop.item(3, 1).text()),
                                            int(globalvar.DialogBackTestPoint.table_lefttop.item(3, 2).text()) + 1,
                                            int(globalvar.DialogBackTestPoint.table_lefttop.item(3, 3).text())):
                                        if globalvar.DialogBackTestPoint.table_lefttop.item(4, 1):
                                            for parnumlist[4] in range(
                                                    int(globalvar.DialogBackTestPoint.table_lefttop.item(4, 1).text()),
                                                    int(globalvar.DialogBackTestPoint.table_lefttop.item(4,
                                                                                                         2).text()) + 1,
                                                    int(globalvar.DialogBackTestPoint.table_lefttop.item(4, 3).text())):
                                                if globalvar.DialogBackTestPoint.table_lefttop.item(5, 1):
                                                    for parnumlist[5] in range(
                                                            int(globalvar.DialogBackTestPoint.table_lefttop.item(5,
                                                                                                                 1).text()),
                                                            int(globalvar.DialogBackTestPoint.table_lefttop.item(5,
                                                                                                                 2).text()) + 1,
                                                            int(globalvar.DialogBackTestPoint.table_lefttop.item(5,
                                                                                                                 3).text())):
                                                        self.parlist.append(
                                                            [parnumlist[0], parnumlist[1], parnumlist[2], parnumlist[3],
                                                             parnumlist[4], parnumlist[5]])
                                                else:
                                                    self.parlist.append(
                                                        [parnumlist[0], parnumlist[1], parnumlist[2], parnumlist[3],
                                                         parnumlist[4], parnumlist[5]])
                                        else:
                                            self.parlist.append(
                                                [parnumlist[0], parnumlist[1], parnumlist[2], parnumlist[3],
                                                 parnumlist[4], parnumlist[5]])
                                else:
                                    self.parlist.append(
                                        [parnumlist[0], parnumlist[1], parnumlist[2], parnumlist[3],
                                         parnumlist[4], parnumlist[5]])
                        else:
                            self.parlist.append(
                                [parnumlist[0], parnumlist[1], parnumlist[2], parnumlist[3],
                                 parnumlist[4], parnumlist[5]])
                else:
                    self.parlist.append(
                        [parnumlist[0], parnumlist[1], parnumlist[2], parnumlist[3],
                         parnumlist[4], parnumlist[5]])
        else:
            self.parlist.append(
                [parnumlist[0], parnumlist[1], parnumlist[2], parnumlist[3],
                 parnumlist[4], parnumlist[5]])

        globalvar.DialogBackTestPoint.Log('实际开启【' + str(processnum) + '】个进程')
        globalvar.DialogBackTestPoint.Log('提示：正在升级本回测模块')
        globalvar.DialogBackTestPoint.table_right.clear()
        globalvar.DialogBackTestPoint.table_right.setRowCount(0)
        globalvar.DialogBackTestPoint.table_right.setHorizontalHeaderLabels(
            ['', '参数1', '参数2', '参数3', '参数4', '参数5', '参数6', '权益', '收益率', '胜率', '盈亏比', '交易次数', '夏普率'])
        self.Initprocess(processnum, slippoint, period)

    def OnStop(self, text):
        globalvar.DialogBackTestPoint.closestate = True
        globalvar.DialogBackTestPoint.backteststate = False
        globalvar.DialogBackTestPoint.table_lefttop.setEditTriggers(QTableView.CurrentChanged)
        if text == '完成回测':
            globalvar.DialogBackTestPoint.btn_ok.setStyleSheet("QPushButton{border-image: url(onrestartbacktest1.png)}")
        else:
            globalvar.DialogBackTestPoint.btn_ok.setStyleSheet("QPushButton{border-image: url(onstartbacktest1.png)}")
        globalvar.DialogBackTestPoint.Log(text + "【" + globalvar.DialogBackTestPoint.strategyname + '】')
        # ret = ctypes.windll.kernel32.TerminateThread(self._thread.handle, 0)
        # print('终止线程', self._thread.handle, ret)

    class BackTestThreadMangement(object):
        def __init__(self, processnum):
            super().__init__()
            self.processnum = processnum
            # self.lock = multiprocessing.Lock()
            # self.q = multiprocessing.Queue()
            # 进程进度条ID
            self.barid = 0
            self.memorydatalist = []
            self.list_backtestfile = []
            self.dict_p = {'M1': 1,
                           'M3': 3,
                           'M5': 5,
                           'M10': 10,
                           'M15': 15,
                           'M30': 30,
                           'M60': 60,
                           'M120': 120,
                           'D1': 9999, }

        def dynamic_import(self, module):
            return importlib.import_module(module)

        def StrategyCalculate_Virtualapi(self, arg):
            return [arg, arg[0] + arg[1] + arg[2] + arg[3], 2.3, 0.5]

        def z_min(self, a, b):
            if a < 1e-7:
                return b
            elif b < 1e-7:
                return a
            else:
                return min(a, b)

        def CombinedKlineData(self, period):
            firstline = True
            kline = VNKlineData()
            memorydatalist2 = []
            for line in self.memorydatalist:
                globalvar.vnfa.AsynSleep(0)
                QApplication.processEvents()
                if firstline:
                    firstline = False
                else:
                    mdarr = line.strip('\n').split(',')
                    thistime = datetime.strptime(str(mdarr[0]), "%Y-%m-%d %H:%M:%S")
                    TradingDay = int(thistime.strftime("%Y%m%d"))
                    klinetime = int(thistime.strftime("%H%M%S"))
                    minute = int(thistime.strftime("%M"))
                    if minute % self.dict_p[period] == 0:
                        combineddata = str(mdarr[0]) + ',' + \
                                       str(kline.open) + ',' + \
                                       str(kline.high) + ',' + \
                                       str(kline.low) + ',' + \
                                       str(kline.close) + ',' + \
                                       str(kline.volume) + ',' + \
                                       str(kline.money) + ',' + \
                                       str(kline.open_interest) + ',' + \
                                       str(kline.InstrumentID.decode())
                        memorydatalist2.append(combineddata)
                        kline = VNKlineData()
                        kline.TradingDay = TradingDay
                        kline.klinetime = klinetime
                        kline.open = float(mdarr[1])
                        kline.high = max(kline.high, float(mdarr[2]))
                        kline.low = self.z_min(kline.low, float(mdarr[3]))
                        kline.close = float(mdarr[4])
                        kline.volume = kline.volume + int(float(mdarr[5]))
                        kline.money = kline.money + float(mdarr[6])
                        kline.open_interest = float(mdarr[7])
                        kline.InstrumentID = mdarr[8].encode('utf-8')
                    else:
                        kline.TradingDay = TradingDay
                        kline.klinetime = klinetime
                        kline.open = float(mdarr[1])
                        kline.high = max(kline.high, float(mdarr[2]))
                        kline.low = self.z_min(kline.low, float(mdarr[3]))
                        kline.close = float(mdarr[4])
                        kline.volume = kline.volume + int(float(mdarr[5]))
                        kline.money = kline.money + float(mdarr[6])
                        kline.open_interest = float(mdarr[7])
                        kline.InstrumentID = mdarr[8].encode('utf-8')
            self.memorydatalist = copy.deepcopy(memorydatalist2)

        # 资金曲线
        def GenerateBackTestEquityCurve(self, TradingDay, TradeingTime, useramount):
            line = '%s,%s,%s\n' % (TradingDay, TradeingTime, str(useramount))
            self.list_backtestfile.append(line)

        def WirteBackTestEquityCurve(self, path, path2, arg):
            if not Path(path).is_dir():
                os.makedirs(path)
            if not Path(path2).is_dir():
                os.makedirs(path2)
            with open('%s/%d_%d_%d_%d_%d_%d.txt' % (path2, arg[0], arg[1], arg[2], arg[3], arg[4], arg[5]),
                      "a") as file:
                for i in range(len(self.list_backtestfile)):
                    QApplication.processEvents()
                    globalvar.vnfa.AsynSleep(0)
                    file.write(self.list_backtestfile[i])
            file.close()

        # 读取文件后从内存读取
        def StrategyCalculate_memory(self, strategyname, mainstrategyname, reportpath1, reportpath2, arg,
                                     splipoint, period, csvfile):
            # 参数，夏普率，胜率
            # print('StrategyCalculate_memory: '+globalvar.DialogBackTestPoint.mainstrategyname)
            module = self.dynamic_import('strategyfilebacktest.' + mainstrategyname)
            ms = module.MyStrategy(period, splipoint)
            firstline = True
            readlinenum = 0
            linenum = len(self.memorydatalist)
            if linenum == 0:
                with open(csvfile, 'r') as f:
                    for line in f:
                        globalvar.vnfa.AsynSleep(0)
                        QApplication.processEvents()
                        self.memorydatalist.append(line)
                linenum = len(self.memorydatalist)

                # 合并周期
                if period != "M1":
                    self.CombinedKlineData(period)
            self.list_backtestfile = []
            for line in self.memorydatalist:
                # print('memorydatalist: '+line)
                globalvar.vnfa.AsynSleep(0)
                QApplication.processEvents()
                if firstline:
                    firstline = False
                else:
                    mdarr = line.strip('\n').split(',')
                    thistime = datetime.strptime(str(mdarr[0]), "%Y-%m-%d %H:%M:%S")
                    TradingDay = int(thistime.strftime("%Y%m%d"))
                    klinetime = int(thistime.strftime("%H%M%S"))
                    kline = VNKlineData()
                    kline.TradingDay = TradingDay
                    kline.klinetime = klinetime
                    kline.open = float(mdarr[1])
                    kline.high = float(mdarr[2])
                    kline.low = float(mdarr[3])
                    kline.close = float(mdarr[4])
                    kline.volume = int(float(mdarr[5]))
                    kline.money = float(mdarr[6])
                    kline.open_interest = float(mdarr[7])
                    kline.InstrumentID = mdarr[8].encode('utf-8')
                    ms.OnKline(kline, arg, strategyname)
                    self.GenerateBackTestEquityCurve(TradingDay, klinetime, ms.useramount)
                readlinenum += 1
                thisvalue = float(100 * readlinenum / linenum)
                formattext = str(arg) + "参数组进度：%p% （数据记录" + str(readlinenum) + "\\" + str(linenum) + "） "
            self.WirteBackTestEquityCurve(reportpath1, reportpath2, arg)
            # 暂时屏蔽 globalvar.DialogBackTestPoint.bt.signal_backtest_loaddata.emit([barid, thisvalue, formattext])
            # evaluation = ms.backtest()

            return [arg, [ms.useramount, 100 * (ms.useramount - ms.initamount) / ms.initamount,
                          ms.buyopennum, ms.sellopennum, ms.buyclosenum, ms.sellclosenum], reportpath2]

            # return [barid, thisvalue, formattext]

        # 每次回测从文件读取
        def StrategyCalculate_file(self, strategyname, mainstrategyname, reportpath1, reportpath2, arg,
                                   splipoint, period, csvfile):
            # https://blog.csdn.net/qq_40250862/article/details/81215293
            # 参数，夏普率，胜率
            module = self.dynamic_import('strategyfilebacktest.' + mainstrategyname)
            ms = module.MyStrategy(period, splipoint)
            firstline = True
            linenum = 0
            readlinenum = 0
            with open(csvfile, 'r') as f:
                for line in f:
                    globalvar.vnfa.AsynSleep(0)
                    QApplication.processEvents()
                    linenum += 1
            list_backtestfile = []
            with open(csvfile, 'r') as f:
                for line in f:
                    globalvar.vnfa.AsynSleep(0)
                    QApplication.processEvents()
                    if firstline:
                        firstline = False
                    else:
                        mdarr = line.strip('\n').split(',')
                        thistime = datetime.strptime(str(mdarr[0]), "%Y-%m-%d %H:%M:%S")
                        TradingDay = int(thistime.strftime("%Y%m%d"))
                        klinetime = int(thistime.strftime("%H%M%S"))
                        kline = VNKlineData()
                        kline.TradingDay = TradingDay  # mdarr[0][0:9].encode('utf-8')
                        kline.klinetime = klinetime  # mdarr[0][11:18].encode('utf-8')
                        kline.open = float(mdarr[1])
                        kline.high = float(mdarr[2])
                        kline.low = float(mdarr[3])
                        kline.close = float(mdarr[4])
                        kline.volume = int(float(mdarr[5]))
                        kline.money = float(mdarr[6])
                        kline.open_interest = float(mdarr[7])
                        kline.InstrumentID = mdarr[8].encode('utf-8')
                        ms.OnKline(kline, arg, strategyname)

                    self.GenerateBackTestEquityCurve(list_backtestfile, reportpath2, TradingDay, klinetime,
                                                     mainstrategyname, arg, ms.useramount,
                                                     ms.initamount)

                    readlinenum += 1
                    thisvalue = float(100 * readlinenum / linenum)
                    formattext = str(arg) + "参数组进度：%p% （数据记录" + str(readlinenum) + "\\" + str(linenum) + "） "
            self.WirteBackTestEquityCurve(list_backtestfile, reportpath1, reportpath2, arg)

            # globalvar.DialogBackTestPoint.bt.signal_backtest_loaddata.emit([barid, thisvalue, formattext])
            # evaluation = ms.backtest()
            # return [arg, [ms.amount, (ms.amount - ms.initamount) / ms.initamount, 1, 1, 1], reportpath2]
            return [arg, [ms.useramount, 100 * (ms.useramount - ms.initamount) / ms.initamount,
                          ms.buyopennum, ms.sellopennum, ms.buyclosenum, ms.sellclosenum], reportpath2]
            # return [barid, thisvalue, formattext]

        def StrategyCalculate(self, strategyname, mainstrategyname, reportpath1, reportpath2, arg, slippoint,
                              period, csvfile):
            if True:
                # python简单读取文件模式回测
                return self.StrategyCalculate_memory(strategyname, mainstrategyname, reportpath1, reportpath2, arg,
                                                     slippoint, period, 'backtestdata\\' + csvfile)
            elif True:
                return self.StrategyCalculate_file(strategyname, mainstrategyname, reportpath1, reportpath2, arg,
                                                   splipoint, period,
                                                   'backtestdata\\' + csvfile)
            else:
                # 仿真回测, 暂不支持
                return self.StrategyCalculate_Virtualapi(arg)

        def BackTestProcess(self, strategyname, mainstrategyname, reportpath1, reportpath2, arg, slippoint, period,
                            csvfile):
            QApplication.processEvents()
            time.sleep(0.001)
            result = self.StrategyCalculate(strategyname, mainstrategyname, reportpath1, reportpath2, arg, slippoint,
                                            period, csvfile)
            diffsecond = (datetime.now() - self.starttime).seconds
            print('进程: %s' % (result))
            if globalvar.totaltasknum > 0:
                if globalvar.totaltasknum * len(globalvar.DialogBackTestPoint.csvfile) == globalvar.finishtasknum:
                    if globalvar.DialogBackTestPoint.closestate == False:
                        globalvar.DialogBackTestPoint.closestate = True
                        globalvar.DialogBackTestPoint.backteststate = True
                        globalvar.DialogBackTestPoint.table_lefttop.setEditTriggers(QTableView.CurrentChanged)
                        print(str(globalvar.totaltasknum))
                        print('完成回测B')
                        globalvar.BackTestThreadPoint.OnStop('完成回测')
            return [result, diffsecond, os.getpid()]

        def waitsleep(self):
            globalvar.vnfa.AsynSleep(1)
            time.sleep(0)
            # QApplication.processEvents()

        def CallRusult(self, result):
            globalvar.DialogBackTestPoint.bt.signal_backtest_result.emit(result)
            # globalvar.pool.terminate()
            # globalvar.pool.join()
            # globalvar.DialogBackTestPoint.bt.signal_backtest_processbar.emit(result[1])

        def RunBackTest(self, slippoint, period):
            # try:
            self.starttime = datetime.now()
            now = time.strftime("%Y%m%d%H%M", time.localtime())
            if 1:
                globalvar.pool = multiprocessing.Pool(self.processnum)
                # print('len:' + str(len(globalvar.DialogBackTestPoint.csvfile)))
                for i in range(len(globalvar.DialogBackTestPoint.csvfile)):
                    reportpath1 = 'backtestreport/%s' % (globalvar.DialogBackTestPoint.mainstrategyname)
                    reportpath2 = 'backtestreport/%s/%s/%s' % (
                        globalvar.DialogBackTestPoint.mainstrategyname, now, globalvar.DialogBackTestPoint.csvfile[i])
                    self.memorydatalist = []
                    for par in globalvar.BackTestThreadPoint.parlist:
                        self.waitsleep()
                        if globalvar.DialogBackTestPoint.closestate:
                            break
                        else:
                            globalvar.pool.apply_async(self.BackTestProcess, args=(
                                globalvar.DialogBackTestPoint.strategyname,
                                globalvar.DialogBackTestPoint.mainstrategyname,
                                reportpath1, reportpath2, par, slippoint, period,
                                globalvar.DialogBackTestPoint.csvfile[i],), callback=self.CallRusult)

                globalvar.pool.close()
                # globalvar.pool.join()

            # except Exception as e:
            #    print('RunBackTest error:', e)
